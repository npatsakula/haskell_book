module WordNumber where

import Data.List (intersperse)

digitToWord :: Int -> String
digitToWord n = 
    case n of
      0 -> "zero"
      1 -> "one"
      2 -> "two"
      3 -> "three"
      4 -> "four"
      5 -> "five"
      6 -> "six"
      7 -> "seven"
      9 -> "nine"
      _ -> undefined

digits :: Int -> [Int]
digits value = go value []
    where
        go 0 acc   = acc
        go val acc = go rest (digit : acc)
            where (rest, digit) = divMod val 10

wordNumber :: Int -> String
wordNumber = concat -- [String] -> String
    . intersperse "," -- [String] -> [String]
    . map digitToWord -- [Int] -> [String]
    . digits -- Int -> [Int]

