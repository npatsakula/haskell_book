module Chapter6 where

-- Trivial structure:
data Trivial = Trivial'
    deriving Show

-- Expected: Trivial' == Trivial' = True
instance Eq Trivial where
    -- (==) :: Trivial -> Trivial -> Bool
    Trivial' == Trivial' = True

-- More complicated data-type:
data DayOfWeek = Mon | Tue | Weds | Thu | Fri | Sat | Sun
    deriving (Ord, Show)

-- Compose!
data Date = Date DayOfWeek Int
    deriving Show

-- Duct tape solution(boily):
instance Eq DayOfWeek where
    (==) Mon Mon   = True
    (==) Tue Tue   = True
    (==) Weds Weds = True
    (==) Thu Thu   = True
    (==) Fri Fri   = True
    (==) Sat Sat   = True
    (==) Sun Sun   = True
    (==) _ _       = False

-- Nested Eq instance:
instance Eq Date where
    (==) (Date weekady dayOfMonth) (Date weekady' dayOfMonth') =
        weekady == weekady' && dayOfMonth == dayOfMonth'

-- Make some ad-hoc polymorphic data-type:
data Identity a = Identity a
    deriving Show

-- instance Eq (Identity a) where ... <-- causes a compilation
-- error due to the fact that 'a' is parametrically polymorphic.
instance Eq a => Eq (Identity a) where
    (==) (Identity x) (Identity x') = x == x'

{-
 - data Ordering = LT | EQ | GT -- defined in GHC.Types
 - 
 - class Eq a => Ord a where
 -      compare :: a -> a -> Ordering
 -      (<) :: a -> a -> Bool
 -      ...
 -      max :: a -> a -> a
 -      min :: a -> a -> a
-}


{-
 - pred -- predecessor
 - succ -- successor
 -
 -
 -class Enum a where
 -  succ :: a -> a
 -  pred :: a -> a
 -  toEnum :: Int -> a
 -  fromEnum :: a -> Int
 -  enumFrom :: a -> [a] -- ex: enumFromTo 3 8 -> [3, 4, 5, 6, 7, 8]
 -  enumFromThen :: a -> a -> [a]
 -  enumFromTo :: a -> a -> [a]
 -  enumFromThenTo :: a -> a -> a -> [a]
 -  {-# MINIMAL toEnum, fromEnum #-}
-}

data Rocks = Rocks String
    deriving (Show, Eq)

data Yeah = Yeah Bool
    deriving (Eq, Show)

data Papu = Papu Rocks Yeah
    deriving (Eq, Show)

newtype Nada = Nada Double deriving (Eq, Show)

instance Num Nada where
    (Nada x) + (Nada y) = Nada $ x + y
    (Nada x) - (Nada y) = Nada $ x - y
    (Nada x) * (Nada y) = Nada $ x * y
    negate (Nada x) = Nada $ negate x
    abs (Nada x) = Nada $ abs x
    signum (Nada x) = Nada $ signum x
    fromInteger x = Nada $ fromInteger x

instance Fractional Nada where
    (Nada x) / (Nada y) = Nada $ x / y
    recip (Nada x) = Nada $ recip x
    fromRational x = Nada $ fromRational x

