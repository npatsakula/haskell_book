module Chapter10 where

-- Note: foldr is RIGHT associative:
-- foldr (+) 0 [1, 2, 3] evaluate as
-- (+) 1 ((+) 2 ((+) 3 0))
myFoldr :: (a -> b -> b) -> b -> [a] -> b
myFoldr f acc lst =
    case lst of
      [] -> acc
      (x:xs) -> f x $ myFoldr f acc xs

mySum :: Num a => [a] -> a
mySum = myFoldr (+) 0

myLength :: [a] -> Int
myLength = myFoldr (\_ acc -> succ acc) 0

myProduct :: Num a => [a] -> a
myProduct = myFoldr (*) 1

myConcat :: [[a]] -> [a]
myConcat = myFoldr (++) []

myAny :: (a -> Bool) -> [a] -> Bool
myAny f = myFoldr (\val acc -> (f val) || acc) False

-- Note: foldl is LEFT associative:
-- foldl (+) 0 [1, 2, 3] evaluate as
-- ((0 + 1) + 2) + 3
myFoldl :: (b -> a -> b) -> b -> [a] -> b
myFoldl _ acc [] = acc
myFoldl f acc (x:xs) = myFoldl f (f acc x) xs

-- foldr (^) 2 [1..3] is equal '1', evaluate as:
-- 1 ^ (2 ^ (3 ^ 2)) = 1
-- foldl (^) 2 [1..3] is equal '64', evaluate as:
-- (2 ^ 1) ^ 2 ^ 3 = 64
-- foldr (:) [] [1..3]
-- 1 : 2 : 3 : [] = [1, 2, 3]
-- foldl (:) [] [1..3] <-- error
-- (:) ((:) ((:) [] 1) 2) 3 <-- can't use without flip:
-- foldl (flip (:)) [] [1..3] = [3, 2, 1]
-- flip :: (a -> b -> c) -> b -> a -> c
-- Associative **is matter**!
--
-- foldl (flip (*)) 1 [1..3] evaluate as:
-- (3 * (2 * (1 * 1)))

