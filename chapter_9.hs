module Chapter9 where

-- May throw exceptions!
myTail :: [a] -> [a]
myTail []       = []
myTail (_ : xs) = xs

-- Same!
myHead :: [a] -> a
myHead (x : _) = x

safeTail :: [a] -> Maybe [a]
safeTail []       = Nothing
safeTail (_ : []) = Nothing
safeTail (_ : xs) = Just xs

safeHead :: [a] -> Maybe a
safeHead []      = Nothing
safeHead (x : _) = Just x

eftBool :: Bool -> Bool -> [Bool]
eftBool False True  = [False, True ]
eftBool False False = [False, False]
eftBool True  True  = [True,  True ]
eftBool True  False = []

myTake :: Int -> [a] -> [a]
myTake num xs = go num xs []
    where
        go 0 _ acc = acc
        go _ [] acc = acc
        go n (el : tl) acc = go (n - 1) tl (acc ++ [el])

myDrop :: Int -> [a] -> [a]
myDrop num xs = go num xs
    where
        go 0 acc = acc
        go _ []  = []
        go n (_ : tl) = go (n - 1) tl

mySplitAt :: Int -> [a] -> ([a], [a])
mySplitAt num xs = go num [] xs
    where
        go 0 fsplit ssplit    = (fsplit, ssplit)
        go _ fsplit []        = (fsplit, [])
        go n fsplit (hd : tl) = go (n - 1) (fsplit ++ [hd]) tl

myTakeWhile :: (a -> Bool) -> [a] -> [a]
myTakeWhile g xs = go g xs []
    where
        go _ [] acc = acc
        go f (hd : tl) acc | (f hd) == False = go f tl (acc ++ [hd])
                           | otherwise      = acc

myDropWhile :: (a -> Bool) -> [a] -> [a]
myDropWhile g xs = go g xs
    where
        go _ []            = []
        go f rst@(hd : tl) | (f hd) == True = go f tl
                         | otherwise      = rst

separator :: Eq a => a -> [a] -> [[a]]
separator delimiter source = go delimiter source []
    where
        go _ [] acc = acc
        go del rawRest acc =
            case myDropWhile (del ==) rawRest of
              [] -> acc
              rst -> go del (myDropWhile (del /=) rst) (acc ++ [myTakeWhile (del ==) rst])

myWords :: String -> [String]
myWords source = separator ' ' source

myLines :: String -> [String]
myLines source = separator '\n' source

-- List comprehension example:
-- [x^2 | x <- [1..10]] -- will produce a new list that includes the square of every number
-- from 1 to 10.
-- [x^2 | x <- [1..10], rem x 2 == 0] -- adding predicates.
-- [x^y | x <- [1..5], y <- [2, 3]] -- multiple generators.
-- Output: [1, 1, 4, 8, 9, 27, 16, 64, 25, 125]
-- [x^y | x <- [1..10], y <- [2, 3], x^y < 200] -- another example.
-- [(x, y) | x <- [1, 2, 3], y <- [6, 7]] -- list of tuples.
-- Output: [(1, 6), (1, 7), (2, 6), (2, 7), (3, 6), (3, 7)]

myElem :: Eq a => a -> [a] -> Bool
myElem _ [] = False
myElem el (hd : tl) = if el == hd
                         then True
                         else myElem el tl

removeLovercase :: String -> String
removeLovercase source = [x | x <- source, myElem x ['A'..'Z']]

mySum :: Num a => [a] -> a
mySum [] = 0
mySum (x : xs) = x + mySum xs

myMap :: (a -> b) -> [a] -> [b]
myMap _ [] = []
myMap f (hd : tl) = f hd : myMap f tl

myFilter :: (a -> Bool) -> [a] -> [a]
myFilter _ [] = []
myFilter f (x : xs)
  | f x = x : myFilter f xs
  | otherwise = myFilter f xs

threeMultipliers :: [Int]
threeMultipliers = filter (\x -> mod x 3 == 0) [1..30]

articlesFilter :: String -> [String]
articlesFilter = myFilter (`notElem` ["the", "a", "an"]) . words

myZip :: [a] -> [b] -> [(a, b)]
myZip [] _ = []
myZip _ [] = []
myZip (l:ls) (r:rs) = (l, r) : myZip ls rs

myZipWith :: (a -> b -> c) -> [a] -> [b] -> [c]
myZipWith _ [] _ = []
myZipWith _ _ [] = []
myZipWith f (l:ls) (r:rs) = f l r : myZipWith f ls rs

myZip' :: [a] -> [b] -> [(a, b)]
myZip' = myZipWith (,)

myOr :: [Bool] -> Bool
myOr [] = False
myOr (x:xs) = x || myOr xs

myAny :: (a -> Bool) -> [a] -> Bool
myAny f = myOr . map f

myReverse :: [a] -> [a]
myReverse [] = []
myReverse (x:xs) = myReverse xs ++ [x]

mySquish :: [[a]] -> [a]
mySquish [] = []
mySquish (x:xs) = x ++ mySquish xs

mySquishMap :: (a -> [b]) -> [a] -> [b]
mySquishMap _ [] = []
mySquishMap f (x:xs) = f x ++ mySquishMap f xs

mySquishAgain :: [[a]] -> [a]
mySquishAgain = mySquishMap id

myFoldr :: (a -> b -> b) -> b -> [a] -> b
myFoldr _ b [] = b
myFoldr f b (x:xs) = myFoldr f (f x b) xs

myMaximumBy :: (a -> a -> Ordering) -> [a] -> a
myMaximumBy f [val] = val
myMaximumBy f (x:xs) = myFoldr (\val currMax -> comparator f val currMax) x xs
    where
        comparator :: (a -> a -> Ordering) -> a -> a -> a
        comparator f x y = if f x y == GT then x else y

