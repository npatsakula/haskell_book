module Chapter8 where

fourFactorial :: Integer
fourFactorial = 4 * 3 * 2 * 1

-- This won't work. It never stops.
brokenFactorial :: Integer -> Integer
brokenFactorial n = n * brokenFactorial (n - 1)

factorial :: Integer -> Integer
factorial n =
    case n of
      0 -> 1
      _ -> n * factorial (n - 1)

incTimes :: (Eq a, Num a) => a -> a -> a
incTimes count acc =
    case count of
      0 -> acc
      _ -> incTimes (count - 1) (acc + 1)

applyTimes :: (Eq a, Num a) => a -> (b -> b) -> b -> b
applyTimes count f val =
    case count of 
      0 -> val
      _ -> applyTimes (count - 1) f (f val)

incTimes' :: (Eq a, Num a) => a -> a -> a
incTimes' val count = applyTimes count (+1) val

-- (.) :: (a -> b) -> (c -> a) -> c -> b
applyTimes' :: (Eq a, Num a) => a -> (b -> b) -> b -> b
applyTimes' count f val =
    case count of
      0 -> val
      _ -> f . applyTimes' (count - 1) f $ val

someBottomFunction :: Bool -> Int
someBottomFunction val =
    case val of
      True  -> error "Bottom!"
      False -> 0

-- Partial function(pattern matching non-exhaustive):
someBottomFunction' :: Bool -> Int
someBottomFunction' False = 0

-- Maybe a = Just a | Nothing
fuckBottom :: Bool -> Maybe Int
fuckBottom False = Just 0
fuckBottom _     = Nothing

fibonacci :: Integral a => a -> a
fibonacci 0 = 0
fibonacci 1 = 1
fibonacci n = fibonacci (n - 2) + fibonacci (n - 1)

type Numerator   = Integer
type Denominator = Integer
data DivRes      = DivRes Integer Integer 
    deriving Show

dividedBy :: Numerator -> Denominator -> DivRes
dividedBy num denom = go num denom 0
    where go n d count
            | n < d = DivRes count n
            | otherwise = go (n - d) d (count + 1)

myMultiplier :: (Num a, Eq a) => a -> a -> a
myMultiplier val num = go val num 0
    where go mul n acc
            | n == 0 = acc
            | otherwise = go mul (n - 1) (acc + val)

mySum :: (Eq a, Num a) => a -> a
mySum num =
    case num of
      0 -> 0
      _ -> num + mySum (num - 1)

