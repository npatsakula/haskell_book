module Cipher where

import Data.Char (toLower, toUpper, isLower, isUpper, chr, ord)

capitalFilter :: String -> String
capitalFilter = filter isUpper

capitalizer :: String -> String
capitalizer [] = []
capitalizer (x:xs) = (toUpper x) : xs

captalizeFirst :: String -> Char
captalizeFirst = toUpper . head

capitalizer' :: String -> String
capitalizer' [] = []
capitalizer' (x:xs) = toUpper x : capitalizer xs

kaiserCipher :: Int -> String -> String
kaiserCipher n = map (chr . (\x -> x + n) . ord)

