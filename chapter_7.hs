module Chapter7 where

-- Achtung! :t mTh -> Integer -> Integer -> Integer -> Integer
-- Why?!
-- mTh x y z = x * y * z
-- mTh x y = \z -> x * y * z
-- mTh x = \y -> \z -> x * y * z
mTh = \x -> \y -> \z -> x * y * z

addOneIfOdd :: Integral x => x -> x
addOneIfOdd = \n -> if odd n then n + 1 else n

addFive :: (Integral x) => x -> x -> x
addFive = \x y -> (if x > y then y else x) + 5

mFlip :: (y -> x -> z) -> x -> y -> z
mFlip f x y = f y x

newtype Username = Username String
newtype AccountNumber = AccountNumber Integer

data User = UnregisteredUser
    | RegisteredUser Username AccountNumber

printUser :: User -> IO ()
printUser UnregisteredUser = putStrLn "Unregistered user"
printUser (RegisteredUser (Username name) (AccountNumber acnum))
    = putStrLn $ name ++ " " ++ show acnum

data WherePenguinsLive =
      Galapagos
    | Antarctica
    | Australia
    | SouthAfrica
    | SouthAmerica
    deriving (Eq, Show)

data Penguin = 
    Peng WherePenguinsLive
    deriving (Eq, Show)

isSouthAfrica :: WherePenguinsLive -> Bool
isSouthAfrica SouthAfrica = True
isSouthAfrica _           = False

gimmeWhereTheyLive :: Penguin -> WherePenguinsLive
gimmeWhereTheyLive (Peng place) = place

humbolt   = Peng SouthAmerica
gentoo    = Peng Antarctica
macaroni  = Peng Antarctica
little    = Peng Australia
galapagos = Peng Galapagos

galapagosPenguin :: Penguin -> Bool
galapagosPenguin (Peng Galapagos) = True
galapagosPenguin _                = False

antarcticPenguin :: Penguin -> Bool
antarcticPenguin (Peng Antarctica) = True
antarcticPenguin _                 = False

antarcticOrGalapagos :: Penguin -> Bool
antarcticOrGalapagos peng = 
    (antarcticPenguin peng) || (galapagosPenguin peng)

tuppler :: (a, b) -> (c, d) -> ((a, c), (b, d))
tuppler (a, b) (c, d) = ((a, c), (b, d))

tuppler3 :: (a, b, c) -> (d, e, f) -> ((a, d), (c, f))
tuppler3 (a, b, c) (d, e, f) = ((a, d), (c, f))

pal :: Eq a => [a] -> String
pal xs = case reverse xs == xs of
           True -> "Yes"
           False -> "No"

pal' :: Eq a => [a] -> String
pal' xs = 
    case y of
      True -> "Yes"
      False -> "No"
    where y = xs == reverse xs

greetIfCool :: String -> IO ()
greetIfCool coolness = 
    putStrLn $ case cool of
      True -> "Greet"
      False -> "No Greet"
    where cool = coolness == "frosty yo"

functionC :: Ord a => a -> a -> a
functionC x y =
    case x > y of
      True -> x
      False -> y

ifEvenAdd2 :: Integral a => a -> a
ifEvenAdd2 n =
    case even n of
      True -> n + 2
      False -> n

nums :: Integer -> Integer
nums n =
    case compare n 0 of
      LT -> -1
      GT -> 1
      EQ -> 0

-- flip :: (a -> b -> c) -> b -> a -> c
fSub :: Num a => a -> a -> a
fSub = flip (-)

myFlip :: (a -> b -> c) -> b -> a -> c
myFlip f a b = f b a
-- myFlip f = \a b -> f b a

data Employee = Coder
    | Manager
    | Veep
    | CEO
    deriving (Eq, Ord, Show)

reportBoss :: Employee -> Employee -> IO ()
reportBoss e e' =
    putStrLn $ show e ++ " is the boss of "  ++ show e'

employeeRank :: Employee -> Employee -> IO ()
employeeRank e e' = 
    case compare e e' of
      LT -> reportBoss e' e
      GT -> reportBoss e e'
      EQ -> putStrLn "Neither employee is boss"

employeeRank' :: (Employee -> Employee -> Ordering)
              -> Employee
              -> Employee
              -> IO ()
employeeRank' f e e' =
    case f e e' of
      LT -> reportBoss e' e
      GT -> (flip reportBoss) e' e
      EQ -> putStrLn "Neither employee is boss"

coderRuleCEOsDrool :: Employee -> Employee -> Ordering
coderRuleCEOsDrool Coder Coder = EQ
coderRuleCEOsDrool Coder _     = GT
coderRuleCEOsDrool _     Coder = LT
coderRuleCEOsDrool e     e'    = compare e e'

dodgy :: Num a => a -> a -> a
dodgy x y = x + y * 10

oneIsOne :: Num a => a -> a
oneIsOne = dodgy 1

oneIsTwo :: Num a => a -> a
oneIsTwo = (flip dodgy) 2

myAbs :: Integer -> Integer
myAbs x
  | x < 0 = (-x)
  | otherwise = x

pal'' xs
  | xs == reverse xs = True
  | otherwise = False

-- (.) :: (b -> c) -> (a -> b) -> a -> c
takeFiveFromEnum :: Integral a => a -> [a]
takeFiveFromEnum x = take 5 . enumFrom $ x

takeFiveFromEnum' :: Integral a => a -> [a]
takeFiveFromEnum' = take 5 . enumFrom

takeFiveOddFromEnum :: Integral a => a -> [a]
takeFiveOddFromEnum x = take 5 . filter odd . enumFrom $ x

takeFiveOddFromEnum' :: Integral a => a -> [a]
takeFiveOddFromEnum' = take 5 . filter odd . enumFrom

mySum :: Num a => a -> [a] -> a
mySum x xs = foldr (+) x xs

mySum' :: Num a => a -> [a] -> a
mySum' = foldr (+)

foldBool :: a -> a -> Bool -> a
foldBool x y val = if val then x else y

foldBool' :: a -> a -> Bool -> a
foldBool' x y val =
    case val of
      True -> x
      False -> y

tupplerMatcher :: (a -> b) -> (a, c) -> (b, c)
tupplerMatcher f (a, c) = (f a, c)

roundTrip :: (Show a, Read a) => a -> a
roundTrip = read . show

